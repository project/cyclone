<?php
/**
 * @file
 * Base CycloneBuilder class.
 *
 * All CycloneBuilder classes should inherit this base class.
 *
 */

abstract class CycloneBuilder {

  /**
   * Build
   *
   * Extending classes must implement this method.
   *
   * @param string $type
   *   The builder type to use. eg AegirBuilder
   *
   * @param string $method
   *   The method to call on the builder. eg site_create, site_destroy
   *
   * @param string $site_id
   *   The site which is being operated on.
   *
   * @param string $token
   *   The private token for the site.
   *
   * @param string $target
   *   The target for the site.
   *
   * @param array $provider
   *   Array of provider values to pass to the $type constructor.
   *
   * @param array $params
   *   Array of params to pass to the $method.
   *
   * @param array $extensions
   *   Array of extensions to run.
   *
   * @param string $notify_url
   *   The target for the site.
   *
   * @throws Exception
   *   Throws exceptions and doesn't catch exceptions from called functions.
   */
  abstract public function build($type, $method, $site_id, $token, $target, $provider, $params, $extensions, $notify_url);

}
