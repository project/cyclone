<?php
/**
 * @file
 * Base CycloneProvider class.
 *
 * All CycloneProvider classes should inherit this base class.
 *
 * The site_create and site_destroy methods are the main ones to implement. The build
 * method is helpful for logging the build process.
 */

abstract class CycloneProvider {
  protected $builder;

  /**
   * Constructor
   *
   * @param Builder $builder
   *   The builder object
   */
  function __construct($builder) {
    $this->builder = $builder;
    if(empty($builder)) {
      throw new Exception (t('Builder is empty.'));
    }
  }

  // Abstract methods //////////////////////////////////////////////////////////

  /**
   * Create a site.
   *
   * Extending classes must implement this method.
   *
   * @param Node $site
   *   The site node to create.
   *
   * @throws Exception
   *   Throws exceptions and doesn't catch exceptions from called functions.
   */
  abstract public function site_create($site);

  /**
   * Destroy a site.
   *
   * Extending classes must implement this method.
   *
   * @param Node $site
   *   The site node to destroy
   *
   * @throws Exception
   *   Throws exceptions and doesn't catch exceptions from called functions.
   */
  abstract public function site_destroy($site);

  /**
   * The URL for the created site.
   *
   * The provider must know how to build this URL based on the site target and
   * and any other config which may exist, such as environments and the like.
   *
   * @param Node $site
   *   The site node to provide a URL for.
   *
   * @returns string
   *   A string for the absolute URL.
   */
  abstract public function website_url($site);

  /**
   * Provider params
   *
   * Each provider has params which are specific to it, such as connection info
   * to gain access to the remote.
   *
   * @returns array
   *   An array of supported site features.
   */
  abstract public function provider_params();

  // Static support functions /////////////////////////////////////////
  // These should be abstract really but they can't be so we throw exception.

  /**
   * Supported methods for this provider.
   *
   * Some providers may not fully support a method. If a method is not supported
   * it should be implemented and throw an error. The supported_methods can be used as
   * a way for the user interface to check that a method is supported before
   * trying to call.
   *
   * @returns array
   *   An array of supported methods.
   */
  public static function supported_methods() {
    throw new Exception('Unimplemented');
  }

  /**
   * Supported site source actions
   *
   * During site creation process the site can be created from different source
   * strings. A source string is space delimited. The first component is the
   * "action" and the other components are params.
   *
   * @returns array
   *   An array of supported site source actions.
   */
  public static function supported_site_source_actions(){
    throw new Exception('Unimplemented');
  }

  /**
   * Supported site features
   *
   * During site creation there are some extra provider specific steps which can
   * be taken. At the moment the only extra feature which can be supported is
   * "aliases".
   *
   * @returns array
   *   An array of supported site features.
   */
  public static function supported_site_features(){
    throw new Exception('Unimplemented');
  }


  // Concrete utilities ////////////////////////////////////////////////////////

  /**
   * Builds the method params.
   *
   * @param Node $site
   *   The site to pull the params from.
   *
   * @param string $method
   *   The method for which the params are to apply.
   *
   * @return array
   *   Associative array of name value pairs.
   */
  public function params($site, $method) {
    $lang = $site->language;
    switch ($method) {

      case CYCLONE_METHOD_CREATE:
        // Blank values which can be empty
        $user_name = empty($site->field_cyc_user_name[$lang][0]['value']) ? '':
          $site->field_cyc_user_name[$lang][0]['value'];
        $user_email = empty($site->field_cyc_user_email[$lang][0]['email']) ? '':
          $site->field_cyc_user_email[$lang][0]['email'];
        $user_role = empty($site->field_cyc_user_role[$lang][0]['value']) ? '':
          $site->field_cyc_user_role[$lang][0]['value'];
        $aliases = empty($site->field_cyc_aliases[$lang][0]['value']) ? '':
          $site->field_cyc_aliases[$lang][0]['value'];
        //$expiry = empty($site->field_cyc_expiry[$lang][0]['value']) ? '':
        //  $site->field_cyc_expiry[$lang][0]['value'];
        $payload_url = empty($site->field_cyc_payload_url[$lang][0]['url']) ? '':
          url($site->field_cyc_payload_url[$lang][0]['url'], array('absolute' => TRUE));
        // Expiry: Make sure the timezone is included.
        $expiry = '';
        if (!empty($site->field_cyc_expiry[$lang][0]) && is_array($site->field_cyc_expiry[$lang][0])) {
          $expiry_value = $site->field_cyc_expiry[$lang][0]['value'];
          $expiry_zone = $site->field_cyc_expiry[$lang][0]['timezone'];
          $date = new DateTime($expiry_value, new DateTimeZone($expiry_zone));
          $expiry = $date->format('Y-m-d H:i:sP');
        }
        $params = array(
          'name' => $site->title,
          'email' => $site->field_cyc_email[$lang][0]['email'],
          'source' => $site->field_cyc_source[$lang][0]['value'],
          'aliases' => $aliases,
          'payload_url'=> $payload_url,
          'user_name' => $user_name,
          'user_email' => $user_email,
          'user_role' => $user_role,
          'expiry' => $expiry,
        );
        break;

      case CYCLONE_METHOD_DESTROY:
        $params = array();
        break;

      default:
        $params = array();
    }
    return $params;
  }

  /**
   * Builds the extension params
   *
   * @param Node $site
   *   The site to marshall.
   *
   * @param string $method
   *   The method being used.
   *
   * @return array
   *   Associative array of name value pairs.
   */
  public function extensions($site, $method) {
    $lang = $site->language;
    $extensions = array();
    if (!empty($site->field_cyc_extensions[$lang][0]['value'])) {
      $data = unserialize($site->field_cyc_extensions[$lang][0]['value']);
      if (!empty($data[$method])) {
        $extensions = $data[$method];
      }
    }
    return $extensions;
  }

  /**
   * Do the build with the builder.
   *
   * Utilizes (inherited) methids on the object so that it can prepare variables
   * across all different providers. The providers then have a simple one liner
   * to implement their respective methods.
   */
  function build($site, $method, $type, $ok_status=NULL) {
    $lang = $site->language;
    $site_id = $site->nid;
    $token = $site->field_cyc_token[$lang][0]['value'];
    $target = $site->field_cyc_target[$lang][0]['value'];
    $provider = $this->provider_params();
    $params = $this->params($site, $method);
    $extensions = $this->extensions($site, $method);
    $notify_url = url(CYCLONE_STATUS_NOTIFICATION_URL, array('absolute' => TRUE));
    // Build. This may throw errors.
    $this->builder->build($type, $method, $site_id, $token, $target, $provider, $params, $extensions, $notify_url);
    // Update the site with new status
    if(isset($ok_status)) {
      $site->field_cyc_status[$lang][0]['value'] = $ok_status;
      // This hack is to stop double saving out of rules which leads to DB
      // errors on duplicate node ids.
      if (!empty($site->nid)) {
        $site->is_new = FALSE;
      }
      node_save($site);
    }
  }

}
