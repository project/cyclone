<?php
/**
 * @file
 * Code for Cyclone rules.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function cyclone_rules_event_info() {
  return array(
    'cyclone_site_notification' => array(
      'label' => t('A site notification occurred'),
      'group' => t('Cyclone'),
      'module' => 'cyclone',
      'variables' => array(
        'cyc_site' => array('type' => 'node', 'label' => t('The site being notified.')),
        'cyc_notification' => array('type' => 'field_collection_item', 'label' => t('The notification to the site.')),
        'login_url' => array('type' => 'text', 'label' => t('The URL used for login.')),
      ),
    ),
  );
}
