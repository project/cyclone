<?php

/**
 * @file
 * Utility function for notifications.
 */

/**
 * Returns a signature for a name value array of values.
 *
 * The $payload is sorted alphabetically by name, to get a canonical
 * representation before being concatenated and hashed.
 *
 * @param array $payload
 *   Associative array of name value pairs.
 *
 * @param string $secret
 *   The secret shared with the remote server accessing the notification
 *   service.
 *
 * @return string
 *   The hash representing the signature.
 */
function cyclone_signature($payload, $secret) {
  return md5(cyclone_signature_candidate($payload) . ';' . $secret);
}

/**
 * Helper which builds the string to be hashed.
 *
 * Helpful for use in logging payloads with problem signatures.
 */
function cyclone_signature_candidate($payload) {
  ksort($payload);
  $parts = array();
  foreach ($payload as $name => $value) {
    $parts[] = "$name:$value";
  }
  return implode(';', $parts);
}

/**
 * Returns a ($site, $notification) array after processing the values.
 *
 *
 * @param array $values
 *   Associative array of name value pairs.
 *
 * @return array
 *   The ($site, $notification) created from the handling of the values
 *
 * @throws Exception
 *   Throws errors can be caught and handles as response codes.
 *
 */
function cyclone_process_notification($processor, $values) {
  // Host.
  $host = ip_address();
  // Check that required field are there.
  // The normal form validation will catch these for people using the UI. We are
  // being extra strict here for services using the form.
  $message = 'Notify error from %host: Field %field is required.';
  if (empty($values['site_id'])) {
    throw new Exception(t($message, array('%host' => $host, '%field' => 'site_id')));
  }
  if (empty($values['type'])) {
    throw new Exception(t($message, array('%host' => $host, '%field' => 'type')));
  }
  if (empty($values['signature'])) {
    throw new Exception(t($message, array('%host' => $host, '%field' => 'signature')));
  }
  if (empty($values['title'])) {
    throw new Exception(t($message, array('%host' => $host, '%field' => 'title')));
  }
  // Check that site exists.
  $site_id = $values['site_id'];
  if (!$site = node_load($site_id)) {
    $vars = array('%host' => $host, '%id' => $site_id);
    $message = 'Notify error from %host: Site %id does not exist.';
    watchdog('cyclone_process_notification', $message, $vars, WATCHDOG_WARNING);
    throw new Exception(t($message, $vars));
  }
  elseif ($site->type != CYCLONE_SITE_TYPE) {
    $vars = array('%host' => $host, '%id' => $id);
    $message = 'Notify error from %host: Node %id is not a site.';
    watchdog('cyclone_process_notification', $message, $vars, WATCHDOG_WARNING);
    throw new Exception(t($message, $vars));
  }
  // Calculate the expected signature
  // We only consider values which have been sent in the payload. We are
  // therefore free to use the values array, sans the signature and other cruft.
  $signature = $values['signature'];
  $allowed = array('type', 'site_id', 'title', 'build_title', 'build_url', 'data');
  $payload = array_intersect_key($values, array_flip($allowed));
  $lang = $site->language;
  $token = $site->field_cyc_token[$lang][0]['value'];
  $expected = cyclone_signature($payload, $token);
  if ($signature != $expected) {
    $vars = array(
      '%host' => $host,
      '%site_id' => $site_id,
      '%candidate' => cyclone_signature_candidate($payload),
      '%expected' => $expected,
      '%signature' => $signature,
    );
    $details = 'Notify bad signature error from %host: site_id=%site_id, candidate=%candidate, expected_sig=%expected, actual_sig=%signature';
    $message = 'Incorrect signature from %host. Please confirm your token and hash algorithm. Check the candidate against the string you built to make the erroneous signature: candidate=%candidate, signature=%signature';
    watchdog('cyclone_process_notification', $details, $vars, WATCHDOG_WARNING);
    throw new Exception(t($message, $vars));
  }
  // Required values.
  $type = $values['type'];
  $title = $values['title'];
  // Optional values
  $build_title = empty($values['build_title']) ? '' : $values['build_title'];
  $build_url = empty($values['build_url']) ? '' : $values['build_url'];
  $data = empty($values['data']) ? array() : json_decode($values['data'], TRUE);
  // Create a Notification Field Collection.
  $notification = cyclone_site_notify($site, $processor, $type, $title, $build_title, $build_url, $data);
  // Return the site and notification
  return array($site, $notification);
}

/**
 * Helper to return a minimal HTML page with status code.
 *
 * This makes it much easier for calling scripts to check the response code,
 * rather than parse a 200 page for error strings.
 */
function cyclone_notify_out($content, $status = '400 Bad Request') {
  drupal_add_http_header('Status', $status);
  print "<html><head><title>$status</title></head><body>$content</body></html>";
  exit();
}
