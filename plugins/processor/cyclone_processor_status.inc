<?php

/**
 * @file
 * Webhook Processor to process status notifications.
 */

$plugin = array(
  'title' => t('Cyclone Status Processor'),
  'handler' => array(
    'class' => 'CycloneStatusProcessor',
  ),
);
