<?php

/**
 * @file
 * Process status notifications.
 */

/**
 * Process status notifications class.
 */
class CycloneStatusProcessor {

  /**
   * Configuration form.
   */
  public function config_form() {
    // No configuration needed.
    return array();
  }

  /**
   * Processes data.
   *
   * This is accepting $data as a array as that is what is being sent from the
   * unserializer.
   */
  public function process($data) {
    // The notify inc file has somm helpful functions for us
    module_load_include('inc', 'cyclone', 'cyclone.notify');

    // Process notification
    try {
      list($site, $notification) = cyclone_process_notification('cyclone_processor_status', $data);
    }
    catch (Exception $e) {
      cyclone_notify_out($e->getMessage());
    }

    // Process status
    // Language. Notification is a field_collection_item and therefore always
    // has und. The site, however, can be translated.
    $lang = LANGUAGE_NONE;
    $site_lang = $site->language;
    // Data:  Data will hold login_url if the method is "create"
    $data = array();
    if(!empty($notification->field_cyc_notification_data[$lang][0]['value'])) {
      $data = unserialize($notification->field_cyc_notification_data[$lang][0]['value']);
    }
    // Type
    $type = $notification->field_cyc_notification_type[$lang][0]['value'];
    // Update the site
    $site_updated = FALSE;
    // Site status
    if (array_key_exists($type, cyclone_known_statuses())) {
      $site->field_cyc_status[$site_lang][0]['value'] = $type;
      $site_updated = TRUE;
    }
    // Site website URL
    // The providers know how to create this and so we instantiate the provider
    // and get the value from there
    if($type == CYCLONE_STATUS_CREATED) {
      try {
        $site->field_cyc_website[$site_lang][0]['url'] = cyclone_site_method($this->site, 'website_url');
        $site_updated = TRUE;
      }
      catch (Exception $e) {
        $message = 'Add website_url failure: site=%name, site_id=%site_id, error=%error';
        $vars = array(
          '%error' => $e->getMessage(),
          '%name' => $this->site->title,
          '%site_id' => $this->site->nid,
        );
        watchdog('cyclone_notification_status', $message, $vars, WATCHDOG_ERROR);
      }
    }
    if ($site_updated) {
      node_save($site);
    }
    // Invoke Rules event.
    // This can be used to send emails out to the owner of the site depending on the status.
    $login_url = empty($data['login_url']) ? '' : $data['login_url'];
    if (module_exists('rules')) {
      rules_invoke_event('cyclone_site_notification', $site, $notification, $login_url);
    }

    // Everything is OK. We return a 200 to show that the notification has been
    // created.
    $message = 'Notification creation success: title=%title, site=%name, site_id=%site_id, type=%type';
    $vars = array(
      '%host' => $host,
      '%name' => $site->title,
      '%site_id' => $site->nid,
      '%type' => $notification->field_cyc_notification_type[$lang][0]['value'],
      '%title' => $notification->field_cyc_notification_title[$lang][0]['value'],
    );
    watchdog('cyclone_notification_status', $message, $vars, WATCHDOG_INFO);
    cyclone_notify_out(t($message, $vars), '200 OK');
  }

}
