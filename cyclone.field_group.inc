<?php
/**
 * @file
 * cyclone.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cyclone_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cyc_backend|node|cyc_site|default';
  $field_group->group_name = 'group_cyc_backend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cyc_site';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Backend',
    'weight' => '2',
    'children' => array(
      0 => 'field_cyc_builder',
      1 => 'field_cyc_provider',
      2 => 'field_cyc_source',
      3 => 'field_cyc_payload_url',
      4 => 'field_cyc_extensions',
      5 => 'field_cyc_expiry',
      6 => 'field_cyc_token',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_cyc_backend|node|cyc_site|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cyc_details|node|cyc_site|default';
  $field_group->group_name = 'group_cyc_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cyc_site';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_cyc_email',
      1 => 'field_cyc_status',
      2 => 'field_cyc_target',
      3 => 'field_cyc_aliases',
      4 => 'field_cyc_website',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_cyc_details|node|cyc_site|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cyc_details|node|cyc_site|form';
  $field_group->group_name = 'group_cyc_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cyc_site';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '3',
    'children' => array(
      0 => 'field_cyc_builder',
      1 => 'field_cyc_email',
      2 => 'field_cyc_provider',
      3 => 'field_cyc_source',
      4 => 'field_cyc_target',
      5 => 'field_cyc_payload_url',
      6 => 'field_cyc_extensions',
      7 => 'field_cyc_expiry',
      8 => 'field_cyc_aliases',
      9 => 'field_cyc_token',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cyc_details|node|cyc_site|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cyc_user|node|cyc_site|default';
  $field_group->group_name = 'group_cyc_user';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cyc_site';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'User',
    'weight' => '1',
    'children' => array(
      0 => 'field_cyc_user_email',
      1 => 'field_cyc_user_name',
      2 => 'field_cyc_user_role',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_cyc_user|node|cyc_site|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cyc_user|node|cyc_site|form';
  $field_group->group_name = 'group_cyc_user';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cyc_site';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'User',
    'weight' => '4',
    'children' => array(
      0 => 'field_cyc_user_email',
      1 => 'field_cyc_user_name',
      2 => 'field_cyc_user_role',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cyc_user|node|cyc_site|form'] = $field_group;

  return $export;
}
