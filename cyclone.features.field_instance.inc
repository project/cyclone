<?php
/**
 * @file
 * cyclone.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function cyclone_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_build'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_build'] = array(
    'bundle' => 'field_cyc_notifications',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A link to the build (if any).',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_build',
    'label' => 'Build',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_created'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_created'] = array(
    'bundle' => 'field_cyc_notifications',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_created',
    'label' => 'Created',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_select',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_data'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_data'] = array(
    'bundle' => 'field_cyc_notifications',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Serialized payload data which is specific to the type.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_data',
    'label' => 'Data',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_ip'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_ip'] = array(
    'bundle' => 'field_cyc_notifications',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_ip',
    'label' => 'IP',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_login'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_login'] = array(
    'bundle' => 'field_cyc_notifications',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The one time login link for a newly created site.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_login',
    'label' => 'Login',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'none',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_processor'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_processor'] = array(
    'bundle' => 'field_cyc_notifications',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The processor which handled the notification.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_processor',
    'label' => 'Processor',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_title'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_title'] = array(
    'bundle' => 'field_cyc_notifications',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The title of the notification.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_title',
    'label' => 'Title',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_notifications-field_cyc_notification_type'
  $field_instances['field_collection_item-field_cyc_notifications-field_cyc_notification_type'] = array(
    'bundle' => 'field_cyc_notifications',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A machine name for the type of notification which should match known statuses to advance the site status.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_notification_type',
    'label' => 'Type',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_aliases'
  $field_instances['node-cyc_site-field_cyc_aliases'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Comma separated list of aliases.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_aliases',
    'label' => 'Aliases',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_builder'
  $field_instances['node-cyc_site-field_cyc_builder'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The builder for building the site.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 20,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_builder',
    'label' => 'Builder',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_email'
  $field_instances['node-cyc_site-field_cyc_email'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 20,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_email',
    'label' => 'Email',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_expiry'
  $field_instances['node-cyc_site-field_cyc_expiry'] = array(
    'bundle' => 'cyc_site',
    'deleted' => 0,
    'description' => 'The date at which a site expires and will be destroyed.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 24,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_expiry',
    'label' => 'Expiry',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_extensions'
  $field_instances['node-cyc_site-field_cyc_extensions'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Structured serialized data for extensions, keys by method and extension type.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 26,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_extensions',
    'label' => 'Extensions',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_notifications'
  $field_instances['node-cyc_site-field_cyc_notifications'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_notifications',
    'label' => 'Notifications',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_hidden',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_payload_url'
  $field_instances['node-cyc_site-field_cyc_payload_url'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The URL to the payload file, if one exists.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_url',
        'weight' => 23,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_payload_url',
    'label' => 'Payload URL',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'none',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_provider'
  $field_instances['node-cyc_site-field_cyc_provider'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The provider (platform) for this site.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_provider',
    'label' => 'Provider',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_source'
  $field_instances['node-cyc_site-field_cyc_source'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The source site.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 21,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_source',
    'label' => 'Source',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_status'
  $field_instances['node-cyc_site-field_cyc_status'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The site status.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 16,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_status',
    'label' => 'Status',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_target'
  $field_instances['node-cyc_site-field_cyc_target'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The FQDN of the new site. No not include http//:.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 18,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_target',
    'label' => 'Target',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_token'
  $field_instances['node-cyc_site-field_cyc_token'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'default_value_function' => 'cyclone_token_field_default',
    'deleted' => 0,
    'description' => 'A token to uniquely identify a site. Keep this private.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 25,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_token',
    'label' => 'Token',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 32,
      ),
      'type' => 'text_textfield',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_user_email'
  $field_instances['node-cyc_site-field_cyc_user_email'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The email address of the created user.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_user_email',
    'label' => 'User email',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_user_name'
  $field_instances['node-cyc_site-field_cyc_user_name'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_user_name',
    'label' => 'User name',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_user_role'
  $field_instances['node-cyc_site-field_cyc_user_role'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The role of the created user.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_user_role',
    'label' => 'User role',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-cyc_site-field_cyc_website'
  $field_instances['node-cyc_site-field_cyc_website'] = array(
    'bundle' => 'cyc_site',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A link to the created website.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_website',
    'label' => 'Website',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'none',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A link to the build (if any).');
  t('A link to the created website.');
  t('A machine name for the type of notification which should match known statuses to advance the site status.');
  t('A token to uniquely identify a site. Keep this private.');
  t('Aliases');
  t('Build');
  t('Builder');
  t('Comma separated list of aliases.');
  t('Created');
  t('Data');
  t('Email');
  t('Expiry');
  t('Extensions');
  t('IP');
  t('Login');
  t('Notifications');
  t('Payload URL');
  t('Processor');
  t('Provider');
  t('Serialized payload data which is specific to the type.');
  t('Source');
  t('Status');
  t('Structured serialized data for extensions, keys by method and extension type.');
  t('Target');
  t('The FQDN of the new site. No not include http//:.');
  t('The URL to the payload file, if one exists.');
  t('The builder for building the site.');
  t('The date at which a site expires and will be destroyed.');
  t('The email address of the created user.');
  t('The one time login link for a newly created site.');
  t('The processor which handled the notification.');
  t('The provider (platform) for this site.');
  t('The role of the created user.');
  t('The site status.');
  t('The source site.');
  t('The title of the notification.');
  t('Title');
  t('Token');
  t('Type');
  t('User email');
  t('User name');
  t('User role');
  t('Website');

  return $field_instances;
}
