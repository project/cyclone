<?php

/**
 * @file
 * Administration page callbacks.
 */

/**
 * Callback for cyclone_site_create_form.
 */
function cyclone_site_create_form($form, &$form_state, $node) {
  $form['_site'] = array(
    '#type' => 'value',
    '#value' => $node,
  );
  return confirm_form(
    $form,
    'Are you sure you wish to create the site?',
    'node/' . $node->nid,
    t('This action cannot be undone.'),
    t('Create'),
    t('Cancel')
  );
}

/**
 * Submit handler for cyclone_site_create_form.
 */
function cyclone_site_create_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  if ($form_state['values']['confirm']) {
    $site = $form_state['values']['_site'];
    $built = FALSE;
    try {
      cyclone_site_method($site, CYCLONE_METHOD_CREATE);
      $built = TRUE;
    }
    catch (Exception $e) {
      $err = $e->getMessage();
    }
    if (!empty($err)) {
      watchdog('cyclone_site_create_form_submit', $err . '. @nid', array('@nid' => $site->nid), WATCHDOG_ERROR);
      drupal_set_message(filter_xss($err), 'error');
    }
    else {
      watchdog('cyclone_site_create_form_submit', 'Site build create OK. @nid', array('@nid' => $site->nid), WATCHDOG_NOTICE);
      drupal_set_message(t('The site create process has started.'));
    }
    drupal_goto('node/' . $site->nid);
  }
}

/**
 * Callback for cyclone_site_destroy_form.
 */
function cyclone_site_destroy_form($form, &$form_state, $node) {
  $form['_site'] = array(
    '#type' => 'value',
    '#value' => $node,
  );
  return confirm_form(
    $form,
    'Are you sure you wish to destroy the site?',
    'node/' . $node->nid,
    t('This action cannot be undone.'),
    t('Destroy'),
    t('Cancel')
  );
}

/**
 * Submit handler for cyclone_site_destroy_form.
 */
function cyclone_site_destroy_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  if ($form_state['values']['confirm']) {
    $site = $form_state['values']['_site'];
    if (cyclone_actions_destroy_site($site)) {
      drupal_set_message(t('The site destroy process has started.'));
    }
    else {
      drupal_set_message(t('There was a problem starting the site destroy process.'));
    }
    drupal_goto('node/' . $site->nid);
  }
}
