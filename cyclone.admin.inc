<?php

/**
 * @file
 * Administration page callbacks.
 */

/**
 * Callback for the builders form.
 */
function cyclone_builders($form, &$form_state) {
  return system_settings_form($form);
}

/**
 * Callback for the providers form.
 */
function cyclone_providers($form, &$form_state) {
  return system_settings_form($form);
}
