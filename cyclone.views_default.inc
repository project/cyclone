<?php
/**
 * @file
 * cyclone.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cyclone_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cyclone_site_notifications';
  $view->description = 'Lists notifications for a site.';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Site notifications';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer cyclone';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_cyc_notification_processor' => 'field_cyc_notification_processor',
    'field_cyc_notification_type' => 'field_cyc_notification_type',
    'field_cyc_notification_title' => 'field_cyc_notification_title',
    'field_cyc_notification_build' => 'field_cyc_notification_build',
    'field_cyc_notification_ip' => 'field_cyc_notification_ip',
    'field_cyc_notification_created' => 'field_cyc_notification_created',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_cyc_notification_processor' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cyc_notification_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cyc_notification_title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cyc_notification_build' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cyc_notification_ip' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cyc_notification_created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Field collection item: Entity with the Notifications (field_cyc_notifications) */
  $handler->display->display_options['relationships']['field_cyc_notifications_node']['id'] = 'field_cyc_notifications_node';
  $handler->display->display_options['relationships']['field_cyc_notifications_node']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_cyc_notifications_node']['field'] = 'field_cyc_notifications_node';
  $handler->display->display_options['relationships']['field_cyc_notifications_node']['required'] = TRUE;
  /* Field: Field collection item: Processor */
  $handler->display->display_options['fields']['field_cyc_notification_processor']['id'] = 'field_cyc_notification_processor';
  $handler->display->display_options['fields']['field_cyc_notification_processor']['table'] = 'field_data_field_cyc_notification_processor';
  $handler->display->display_options['fields']['field_cyc_notification_processor']['field'] = 'field_cyc_notification_processor';
  $handler->display->display_options['fields']['field_cyc_notification_processor']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Type */
  $handler->display->display_options['fields']['field_cyc_notification_type']['id'] = 'field_cyc_notification_type';
  $handler->display->display_options['fields']['field_cyc_notification_type']['table'] = 'field_data_field_cyc_notification_type';
  $handler->display->display_options['fields']['field_cyc_notification_type']['field'] = 'field_cyc_notification_type';
  $handler->display->display_options['fields']['field_cyc_notification_type']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Title */
  $handler->display->display_options['fields']['field_cyc_notification_title']['id'] = 'field_cyc_notification_title';
  $handler->display->display_options['fields']['field_cyc_notification_title']['table'] = 'field_data_field_cyc_notification_title';
  $handler->display->display_options['fields']['field_cyc_notification_title']['field'] = 'field_cyc_notification_title';
  $handler->display->display_options['fields']['field_cyc_notification_title']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Build */
  $handler->display->display_options['fields']['field_cyc_notification_build']['id'] = 'field_cyc_notification_build';
  $handler->display->display_options['fields']['field_cyc_notification_build']['table'] = 'field_data_field_cyc_notification_build';
  $handler->display->display_options['fields']['field_cyc_notification_build']['field'] = 'field_cyc_notification_build';
  $handler->display->display_options['fields']['field_cyc_notification_build']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cyc_notification_build']['click_sort_column'] = 'url';
  /* Field: Field collection item: IP */
  $handler->display->display_options['fields']['field_cyc_notification_ip']['id'] = 'field_cyc_notification_ip';
  $handler->display->display_options['fields']['field_cyc_notification_ip']['table'] = 'field_data_field_cyc_notification_ip';
  $handler->display->display_options['fields']['field_cyc_notification_ip']['field'] = 'field_cyc_notification_ip';
  $handler->display->display_options['fields']['field_cyc_notification_ip']['element_label_colon'] = FALSE;
  /* Field: Field collection item: Created */
  $handler->display->display_options['fields']['field_cyc_notification_created']['id'] = 'field_cyc_notification_created';
  $handler->display->display_options['fields']['field_cyc_notification_created']['table'] = 'field_data_field_cyc_notification_created';
  $handler->display->display_options['fields']['field_cyc_notification_created']['field'] = 'field_cyc_notification_created';
  $handler->display->display_options['fields']['field_cyc_notification_created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cyc_notification_created']['type'] = 'format_interval';
  $handler->display->display_options['fields']['field_cyc_notification_created']['settings'] = array(
    'interval' => '2',
    'interval_display' => 'time ago',
  );
  /* Sort criterion: Field collection item: Created (field_cyc_notification_created) */
  $handler->display->display_options['sorts']['field_cyc_notification_created_value']['id'] = 'field_cyc_notification_created_value';
  $handler->display->display_options['sorts']['field_cyc_notification_created_value']['table'] = 'field_data_field_cyc_notification_created';
  $handler->display->display_options['sorts']['field_cyc_notification_created_value']['field'] = 'field_cyc_notification_created_value';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'field_cyc_notifications_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';

  /* Display: EVA Field */
  $handler = $view->new_display('entity_view', 'EVA Field', 'entity_view_1');
  $handler->display->display_options['entity_type'] = 'node';
  $handler->display->display_options['bundles'] = array(
    0 => 'cyc_site',
  );
  $translatables['cyclone_site_notifications'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('field_cyc_notifications'),
    t('Processor'),
    t('Type'),
    t('Title'),
    t('Build'),
    t('IP'),
    t('Created'),
    t('All'),
    t('EVA Field'),
  );
  $export['cyclone_site_notifications'] = $view;

  $view = new view();
  $view->name = 'cyclone_sites';
  $view->description = 'Lists sites the user owns.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Sites';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'My Sites';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_cyc_target' => 'field_cyc_target',
    'field_cyc_status' => 'field_cyc_status',
    'created' => 'created',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cyc_target' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cyc_status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  /* Field: Bulk operations: Content */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::node_assign_owner_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::cyclone_site_create_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::cyclone_site_destroy_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_sticky_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_unsticky_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::node_promote_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_publish_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpromote_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_save_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::panelizer_set_status_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_by_keyword_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::cyclone_site_update_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Site Info';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Target */
  $handler->display->display_options['fields']['field_cyc_target']['id'] = 'field_cyc_target';
  $handler->display->display_options['fields']['field_cyc_target']['table'] = 'field_data_field_cyc_target';
  $handler->display->display_options['fields']['field_cyc_target']['field'] = 'field_cyc_target';
  $handler->display->display_options['fields']['field_cyc_target']['label'] = 'Actual website';
  $handler->display->display_options['fields']['field_cyc_target']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_cyc_target']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_cyc_target']['alter']['path'] = '[field_cyc_target]';
  $handler->display->display_options['fields']['field_cyc_target']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_cyc_target']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_cyc_target']['element_label_colon'] = FALSE;
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_cyc_status']['id'] = 'field_cyc_status';
  $handler->display->display_options['fields']['field_cyc_status']['table'] = 'field_data_field_cyc_status';
  $handler->display->display_options['fields']['field_cyc_status']['field'] = 'field_cyc_status';
  $handler->display->display_options['fields']['field_cyc_status']['element_label_colon'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'raw time ago';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Author uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'node';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cyc_site' => 'cyc_site',
  );
  /* Filter criterion: Content: Status (field_cyc_status) */
  $handler->display->display_options['filters']['field_cyc_status_value']['id'] = 'field_cyc_status_value';
  $handler->display->display_options['filters']['field_cyc_status_value']['table'] = 'field_data_field_cyc_status';
  $handler->display->display_options['filters']['field_cyc_status_value']['field'] = 'field_cyc_status_value';
  $handler->display->display_options['filters']['field_cyc_status_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cyc_status_value']['expose']['operator_id'] = 'field_cyc_status_value_op';
  $handler->display->display_options['filters']['field_cyc_status_value']['expose']['operator'] = 'field_cyc_status_value_op';
  $handler->display->display_options['filters']['field_cyc_status_value']['expose']['identifier'] = 'field_cyc_status_value';
  $handler->display->display_options['filters']['field_cyc_status_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: User Sites Page */
  $handler = $view->new_display('page', 'User Sites Page', 'page');
  $handler->display->display_options['path'] = 'user/%/sites';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Sites';
  $handler->display->display_options['menu']['description'] = 'A list of sites launched for you.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Admin Site Page */
  $handler = $view->new_display('page', 'Admin Site Page', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Cyclone';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer cyclone';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Bulk operations: Content */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::node_assign_owner_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::cyclone_site_create_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::cyclone_site_destroy_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_sticky_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_unsticky_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::node_promote_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_publish_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpromote_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_save_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::panelizer_set_status_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_by_keyword_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Site Info';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Target */
  $handler->display->display_options['fields']['field_cyc_target']['id'] = 'field_cyc_target';
  $handler->display->display_options['fields']['field_cyc_target']['table'] = 'field_data_field_cyc_target';
  $handler->display->display_options['fields']['field_cyc_target']['field'] = 'field_cyc_target';
  $handler->display->display_options['fields']['field_cyc_target']['label'] = 'Actual website';
  $handler->display->display_options['fields']['field_cyc_target']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_cyc_target']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_cyc_target']['alter']['path'] = '[field_cyc_target]';
  $handler->display->display_options['fields']['field_cyc_target']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_cyc_target']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_cyc_target']['element_label_colon'] = FALSE;
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_cyc_status']['id'] = 'field_cyc_status';
  $handler->display->display_options['fields']['field_cyc_status']['table'] = 'field_data_field_cyc_status';
  $handler->display->display_options['fields']['field_cyc_status']['field'] = 'field_cyc_status';
  $handler->display->display_options['fields']['field_cyc_status']['element_label_colon'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'raw time ago';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['path'] = 'admin/config/services/cyclone';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Cyclone';
  $handler->display->display_options['menu']['weight'] = '49';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['cyclone_sites'] = array(
    t('Master'),
    t('My Sites'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('author'),
    t('Site Info'),
    t('Actual website'),
    t('Status'),
    t('Created'),
    t('All'),
    t('User Sites Page'),
    t('Admin Site Page'),
    t('Cyclone'),
  );
  $export['cyclone_sites'] = $view;

  return $export;
}
