<?php
/**
 * @file
 * Demonstrates Cyclone hooks.
 */

/**
 * Alter the $site node object just before it is passed off to the builder $method.
 *
 * This hook gives implementing modules the chance to make alterations to
 * variables such as field_cyc_extensions and possibly field_cyc_params.
 *
 * @param node $site
 *   The cyc_site node which is used for the build.
 *
 * @param string $method
 *   The builder method which is just about to be called.
 */
function hook_cyclone_build_site($site, $method) {}

/**
 * Check to see whether a $source string can be used to create a new site.
 *
 * This hook gives implementing modules the chance to stop a user creating a
 * new site based on the source. It is therefore possible to restrict the current
 * user according to absolute or relative limits.
 *
 * @param string $source
 *   The site source which may be limited.
 *
 * @returns boolean
 *   Flag which indicates whether the source is allowed to be created.
 */
function hook_cyclone_source_create_allowed($source) {}
