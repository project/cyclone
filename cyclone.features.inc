<?php
/**
 * @file
 * cyclone.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cyclone_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "webhook" && $api == "webhook") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cyclone_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cyclone_node_info() {
  $items = array(
    'cyc_site' => array(
      'name' => t('Site'),
      'base' => 'node_content',
      'description' => t('A site which has been built on an external provider.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
