Cyclone Aegir Provider Drupal Module
====================================

Implements a cyclone_provider plugin for the Aegir hosting services.

Installation
------------

Cyclone Aegir Provider is packaged as a sub module of the Cyclone project.
Enable it on the modules (admin/build/modules) page.

Follow the Configure link (admin/config/services/cyclone/providers) and enter
your authentication information for the provider.

You must have your own account already set up at an Aegir provider.

Basic usage
-----------

Once setup, the Aegir provider will become available on cyc_site and
cyc_temlate pages, as well as anywhere else a provider needs to be selected.

Maintainers
-----------

- Murray Woodman
- Marji Cermak
- Ivan Zugec
