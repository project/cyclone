<?php
/**
 * @file
 * Aegir Provider.
 */

/**
 * Defines the Aegir Provider plugin and the handler to use.
 */
$plugin = array(
  'name' => 'cyclone_provider_aegir',
  'label' => t('Aegir Provider'),
  'description' => t("Manages sites on Aegir systems."),
  'handler' => array(
    'class' => 'CycloneAegirProvider',
  ),
);
