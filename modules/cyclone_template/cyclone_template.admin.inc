<?php

/**
 * @file
 * Administration page callbacks.
 */

/**
 * Callback for the settings form.
 */
function cyclone_template_settings($form, &$form_state) {
  $form['cyclone_template_show_aliases'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show aliases'),
    '#title' => t('Show aliases field in the create site form.'),
    '#default_value' => variable_get('cyclone_template_show_aliases', ''),
  );
  return system_settings_form($form);
}
