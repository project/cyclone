<?php
/**
 * @file
 * cyclone_template.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function cyclone_template_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_builder'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_builder'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The Builder to use for the build. Builders are distributed as plugins in modules which need to be enabled.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_builder',
    'label' => 'Builder',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_email'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_email'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'default_value_function' => 'cyclone_email_field_default',
    'deleted' => 0,
    'description' => 'The email address to be assigned to the site.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_email',
    'label' => 'Email',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_payload'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_payload'] = array(
    'bundle' => 'field_cyc_template',
    'deleted' => 0,
    'description' => 'A private payload suitable for archives (.gz) and build (.build) files.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_payload',
    'label' => 'Payload',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'cyc-payload',
      'file_extensions' => 'gz build',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_provider'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_provider'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The Provider to use for the build. Providers are distributed as plugins in modules which need to be enabled.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_provider',
    'label' => 'Provider',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_source'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_source'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The source platform (provision), site (clone) or make file (make). eg. "clone template.example.com", "provision drupal7", "make http://example.com/template.make"',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_source',
    'label' => 'Source',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_target_domain'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_target_domain'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The base domain for the eventual target website. eg. example.com. You may set wildcard DNS to this value.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_target_domain',
    'label' => 'Target domain',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_target_prefix'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_target_prefix'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A string to be prepended to the target name.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_target_prefix',
    'label' => 'Target prefix',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_target_suffix'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_target_suffix'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A string to be appended to the target name.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_target_suffix',
    'label' => 'Target suffix',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_ttl'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_ttl'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'The time to live for a created site before it is automatically destroyed.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_ttl',
    'label' => 'TTL',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'timeperiod',
      'settings' => array(
        'units' => array(
          1 => array(
            'max' => 0,
            'step size' => 1,
          ),
          60 => array(
            'max' => 55,
            'step size' => 5,
          ),
          3600 => array(
            'max' => 23,
            'step size' => 1,
          ),
          86400 => array(
            'max' => 90,
            'step size' => 1,
          ),
        ),
      ),
      'type' => 'timeperiod_select',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_cyc_template-field_cyc_user_role'
  $field_instances['field_collection_item-field_cyc_template-field_cyc_user_role'] = array(
    'bundle' => 'field_cyc_template',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The user role granted to the creator on the target website.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_cyc_user_role',
    'label' => 'User role',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-cyc_template_display-body'
  $field_instances['node-cyc_template_display-body'] = array(
    'bundle' => 'cyc_template_display',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-cyc_template_display-field_cyc_template'
  $field_instances['node-cyc_template_display-field_cyc_template'] = array(
    'bundle' => 'cyc_template_display',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The template which holds the data needed for making sites.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cyc_template',
    'label' => 'Template',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => -3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A private payload suitable for archives (.gz) and build (.build) files.');
  t('A string to be appended to the target name.');
  t('A string to be prepended to the target name.');
  t('Body');
  t('Builder');
  t('Email');
  t('Payload');
  t('Provider');
  t('Source');
  t('TTL');
  t('Target domain');
  t('Target prefix');
  t('Target suffix');
  t('Template');
  t('The Builder to use for the build. Builders are distributed as plugins in modules which need to be enabled.');
  t('The Provider to use for the build. Providers are distributed as plugins in modules which need to be enabled.');
  t('The base domain for the eventual target website. eg. example.com. You may set wildcard DNS to this value.');
  t('The email address to be assigned to the site.');
  t('The source platform (provision), site (clone) or make file (make). eg. "clone template.example.com", "provision drupal7", "make http://example.com/template.make"');
  t('The template which holds the data needed for making sites.');
  t('The time to live for a created site before it is automatically destroyed.');
  t('The user role granted to the creator on the target website.');
  t('User role');

  return $field_instances;
}
