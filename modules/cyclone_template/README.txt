Cyclone Template Drupal Module
==============================

Cyclone Template is an important module which sits on top of Cyclone and
provides the mechanism to easily create sites from a single template. It
provides a cyclone_template_display content type and a cyclone_template
(field_collection_item).

A create site form is exposed as a block. The block takes the template, combines
it with the user entered data and then creates a cyc_site node. This then
launches the site build process.

Installation
------------
Cyclone Template  is packaged as a sub module of the Cyclone project.
Enable it on the modules (admin/build/modules) page.

Basic usage
-----------

Create new Display Template nodes, filling in the template config fields
Users then can create new sites.

Extensions
----------

Site builders can make their own version of a display node and embed the
cyclone_template field collection via a cyc_template field. You can then display
the templates as you wish and still enjoy the functionality the module provides.

Maintainers
-----------
- Murray Woodman
- Marji Cermak
- Ivan Zugec
