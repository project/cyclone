<?php
/**
 * @file
 * cyclone_template.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cyclone_template_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function cyclone_template_node_info() {
  $items = array(
    'cyc_template_display' => array(
      'name' => t('Cyclone template display'),
      'base' => 'node_content',
      'description' => t('A simple display for a Template.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
