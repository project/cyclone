<?php
/**
 * @file
 * cyclone_template.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cyclone_template_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cyc_target|field_collection_item|field_cyc_template|form';
  $field_group->group_name = 'group_cyc_target';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_cyc_template';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Target',
    'weight' => '4',
    'children' => array(
      0 => 'field_cyc_target_prefix',
      1 => 'field_cyc_target_suffix',
      2 => 'field_cyc_target_domain',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cyc_target|field_collection_item|field_cyc_template|form'] = $field_group;

  return $export;
}
