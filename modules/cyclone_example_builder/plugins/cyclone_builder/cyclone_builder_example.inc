<?php
/**
 * @file
 * Example Builder
 */

/**
 * Defines the Example Builder plugin and the handler to use.
 */
$plugin = array(
  'name' => 'cyclone_builder_example',
  'label' => t('Example Builder'),
  'description' => t("Builds sites with Example builder."),
  'handler' => array(
    'class' => 'CycloneExampleBuilder',
  ),
);
