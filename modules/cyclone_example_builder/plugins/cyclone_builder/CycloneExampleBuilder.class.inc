<?php
/**
 * @file
 * CycloneExampleBuilder class.
 */

class CycloneExampleBuilder extends CycloneBuilder {

  /**
   * Build
   *
   * Extending classes must implement this method.
   *
   * @param string $type
   *   The builder type to use. eg AegirBuilder
   *
   * @param string $method
   *   The method to call on the builder. eg site_create, site_destroy
   *
   * @param string $site_id
   *   The site which is being operated on.
   *
   * @param string $token
   *   The private token for the site.
   *
   * @param string $target
   *   The target for the site.
   *
   * @param array $provider
   *   Array of provider values to pass to the $type constructor.
   *
   * @param array $params
   *   Array of params to pass to the $method.
   *
   * @param array $extensions
   *   Array of extensions to run.
   *
   * @param string $notify_url
   *   The target for the site.
   *
   * @throws Exception
   *   Throws exceptions and doesn't catch exceptions from called functions.
   */
  public function build($type, $method, $site_id, $token, $target, $provider, $params, $extensions, $notify_url) {
    // This is where the action happens.
    // Be sure to throw errors if any occur.
    drupal_set_message(t('No site will be built because this is an Example Builder.'), 'warning');
    // Print out some debug to convenience those implementing their own
    // Provider or custom extensions.
    if (module_exists('devel') && user_access('administer cyclone')) {
      dsm(
        array(
          'type' => $type,
          'method' => $method,
          'site_id' => $site_id,
          'token' => $token,
          'target' => $target,
          'provider' => $provider,
          'params' => $params,
          'extensions' => $extensions,
          'notify_url' => $notify_url
        )
      );
    }
  }

}
