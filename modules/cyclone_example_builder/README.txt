Cyclone Example Builder Drupal Module
=====================================

Cyclone Example Builder is an example builder which is helpful when coding
and debugging other builders. Rather than actually calling code or making an
external request, it just echoes the payload to the screen, if devel module
has been enabled.

Installation
------------

Cyclone Jenkins Builder is packaged as a sub module of the Cyclone project.
Enable it on the modules (admin/build/modules) page.

Enable devel module.

Basic usage
-----------

Create sites and then examine the payload dumped to the screen.

Maintainers
-----------

- Murray Woodman
- Marji Cermak
- Ivan Zugec
