<?php
/**
 * @file
 * CycloneJenkinsBuilder class.
 *
 * Thanks to the "jenkins" project for the foundation of this code.
 */

class CycloneJenkinsBuilder extends CycloneBuilder {

  /**
   * Build
   *
   * Extending classes must implement this method.
   *
   * @param string $type
   *   The builder type to use. eg AegirBuilder
   *
   * @param string $method
   *   The method to call on the builder. eg site_create, site_destroy
   *
   * @param string $site_id
   *   The site which is being operated on.
   *
   * @param string $token
   *   The private token for the site.
   *
   * @param string $target
   *   The target for the site.
   *
   * @param array $provider
   *   Array of provider values to pass to the $type constructor.
   *
   * @param array $params
   *   Array of params to pass to the $method.
   *
   * @param array $extensions
   *   Array of extensions to run.
   *
   * @param string $notify_url
   *   The target for the site.
   *
   * @throws Exception
   *   Throws exceptions and doesn't catch exceptions from called functions.
   */
  public function build($type, $method, $site_id, $token, $target, $provider, $params, $extensions, $notify_url) {
    // Payload for entire request
    $payload = array(
      'type' => $type,
      'method' => $method,
      'site_id' => $site_id,
      'token' => $token,
      'target' => $target,
      'provider' => json_encode($provider),
      'params' => json_encode($params),
      'extensions' => json_encode($extensions),
      'notify_url' => $notify_url,
    );
    // Data from Payload
    $data = array();
    if (is_array($payload)) {
      $data = array('parameter' => array());
      foreach ($payload as $name => $value) {
        $data['parameter'][] = array('name' => $name, 'value' => $value);
      }
    }
    // Init
    $json = 'json=' . json_encode($data);
    $headers = array('Content-Type' => 'application/x-www-form-urlencoded');
    $response = NULL;
    $query = array();
    $method = 'POST';
    // Base URL
    $ver = variable_get('cyclone_jenkins_builder_api_version', CYCLONE_JENKINS_BUILDER_API_VERSION);
    $def = variable_get('cyclone_jenkins_builder_url_base', CYCLONE_JENKINS_BUILDER_URL_BASE) . $ver;
    $url = variable_get('cyclone_jenkins_builder_job_url', $def) . '/build';
    // Options: method
    $options = array(
      'method' => $method,
    );
    // Force request to start immediately.
    if (!isset($query['delay'])) {
      $query['delay'] = '0sec';
    }
    // Check for token
    $builder_token = variable_get('cyclone_jenkins_builder_token', '');
    if (!empty($builder_token)) {
      $query['token'] = $builder_token;
    }
    // Full URL
    $url .= '?'. drupal_http_build_query($query);
    // Add json data to the options
    if ($method == 'POST' && !empty($json)) {
      $options['data'] = $json;
    }
    // Default to JSON unless otherwise specified.
    $default_headers = array(
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
    );
    $headers += $default_headers;
    if (!empty($headers)) {
      $options['headers'] = $headers;
    }
    // Do HTTP request and get response object.
    $response = drupal_http_request($url, $options);
    // DEBUG: The response is very handy for finding out what went wrong.
    //dsm($url); dsm($options); dsm($response);
    // Response code should be something between 200 and 202.
    if (!in_array($response->code, range(200, 202))) {
      throw new Exception(
        t('Jenkins builder: Bad response: %code: %error. Check Jenkins console log.',
          array('%code' => $response->code, '%error' => $response->error)
        )
      );
    }
  }
}
