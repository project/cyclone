<?php
/**
 * @file
 * Jenkins Builder
 */

/**
 * Defines the Jenkins Builder plugin and the handler to use.
 */
$plugin = array(
  'name' => 'cyclone_builder_jenkins',
  'label' => t('Jenkins Builder'),
  'description' => t("Builds sites with Jenkins."),
  'handler' => array(
    'class' => 'CycloneJenkinsBuilder',
  ),
);
