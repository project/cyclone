Cyclone Jenkins Builder Drupal Module
=====================================

Cyclone Jenkins Builder is the primary builder for building sites. The module is
configured with a target Jenkins server which receives the payload in a
parameterized job. The Jenkins server then runs the job and returns the job to
a notifier URL, implemented by the cyclone module.

In order to use this module successfully you must have configured a Jenkins
Server and deployed further scripts to run the job.

Installation
------------
Cyclone Jenkins Builder is packaged as a sub module of the Cyclone project.
Enable it on the modules (admin/build/modules) page.

Basic usage
-----------

Maintainers
-----------

- Murray Woodman
- Marji Cermak
- Ivan Zugec
