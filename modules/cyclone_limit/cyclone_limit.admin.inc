<?php

/**
 * @file
 * Administration page callbacks.
 */

/**
 * Callback for the settings form.
 */
function cyclone_limit_settings($form, &$form_state) {
  $limits = array(
    'cyclone_limit_rate_hour' => t('Rate hour'),
    'cyclone_limit_rate_day' => t('Rate day'),
    'cyclone_limit_rate_week' => t('Rate week'),
    'cyclone_limit_rate_month' => t('Rate month'),
    'cyclone_limit_max_created' => t('Max created'),
    'cyclone_limit_max_all' => t('Max all'),
  );
  foreach ($limits as $key => $title) {
    $form[$key] = array(
      '#type' => 'textfield',
      '#title' => $title,
      '#default_value' => variable_get($key, ''),
      '#element_validate' => array('element_validate_integer_positive'),
    );
  }
  return system_settings_form($form);
}
