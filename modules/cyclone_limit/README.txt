Cyclone Limit Drupal Module
===========================

Cyclone Limit places usage restrictions on users creating sites.

As the site creation process is expensive it is important that resources are not
abused. The module can be configured to limit users for
- hourly rates
- daily rates
- weekly rates
- monthly rates
- max number of published "created" sites
- max number of published sites (which may be created or destroyed)

Installation
------------
Cyclone Limit is packaged as a sub module of the Cyclone project.
Enable it on the modules (admin/build/modules) page.

Follow the Configure link (admin/config/services/cyclone/limit) and enter
your user limits.

Basic usage
-----------

Users are notified if they have exceeded ther limits and are not given access to
site creattion forms.

Maintainers
-----------

- Murray Woodman
- Marji Cermak
- Ivan Zugec
