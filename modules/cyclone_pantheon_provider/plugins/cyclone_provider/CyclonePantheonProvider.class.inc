<?php
/**
 * @file
 * CyclonePatheonProvider class.
 */

class CyclonePantheonProvider extends CycloneProvider {

  // Methods ///////////////////////////////////////////////////////////////////

  /**
   * Create a site with a builder.
   *
   * @param Node $site
   *   The site node to create.
   *
   * @throws Exception
   *   Throws exceptions and doesn't catch exceptions from clled functions.
   */
  public function site_create($site) {
    $this->build($site, CYCLONE_METHOD_CREATE, 'cyclone.builders.Pantheon', CYCLONE_STATUS_CREATE_STARTED);
  }

  /**
   * Destroy a site with a builder.
   *
   * @param Node $site
   *   The site node to destroy.
   *
   * @throws Exception
   *   Throws exceptions and doesn't catch exceptions from called functions.
   */
  public function site_destroy($site) {
    $this->build($site, CYCLONE_METHOD_DESTROY, 'cyclone.builders.Pantheon', CYCLONE_STATUS_DESTROY_STARTED);
  }

  /**
   * The URL for the created site.
   *
   * @param Node $site
   *   The site node to provide a URL for.
   *
   * @returns string
   *   A string for the absolute URL.
   */
  public function website_url($site) {
    // Default (wrong - but a starting point)
    $lang = $site->language;
    $url = $site->field_cyc_target[$lang][0]['value'];
    // Load up the environments and get the last one.
    if ($environments = cyclone_name_pipe_value_lines_to_array(variable_get('cyclone_pantheon_provider_environments', ''))) {
      end($environments);
      $last_env = key($environments);
      $url = 'http://' . $last_env . '-' . $url;
    }
    return $url;
  }

  // Support ///////////////////////////////////////////////////////////////////

  /**
   * Supported methods for this provider.
   *
   * @returns array
   *   An array of supported methods.
   */
  public static function supported_methods() {
    return array(CYCLONE_METHOD_CREATE, CYCLONE_METHOD_DESTROY);
  }

  /**
   * Supported site source actions
   *
   * @returns array
   *   An array of supported site source actions.
   */
  public static function supported_site_source_actions() {
    return array('import', 'install');
  }

  /**
   * Supported site features
   *
   * @returns array
   *   An array of supported site features.
   */
  public static function supported_site_features() {
    return array();
  }

  /**
   * Builds params for the provider.
   *
   * @return array
   *   Associative array of name value pairs.
   */
  public function provider_params() {
    return array (
      'host_string' => variable_get('cyclone_pantheon_provider_host_string', CYCLONE_PANTHEON_PROVIDER_HOST_STRING),
      'environments' => cyclone_name_pipe_value_lines_to_array(
        variable_get('cyclone_pantheon_provider_environments', CYCLONE_PANTHEON_PROVIDER_ENVIRONMENTS)
      ),
    );
  }

}
