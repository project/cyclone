<?php
/**
 * @file
 * Pantheon Provider.
 */

/**
 * Defines the Pantheon Manger plugin and the handler to use.
 */
$plugin = array(
  'name' => 'cyclone_provider_pantheon',
  'label' => t('Pantheon Provider'),
  'description' => t("Manages sites on Pantheon systems."),
  'handler' => array(
    'class' => 'CyclonePantheonProvider',
  ),
);
