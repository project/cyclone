<?php
/**
 * @file
 * cyclone.webhook.inc
 */

/**
 * Implements hook_webhook_default_config().
 */
function cyclone_webhook_default_config() {
  $export = array();

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '1';
  $webhook->title = 'Status notifiction';
  $webhook->machine_name = 'cyclone_notification_status';
  $webhook->description = 'Webhook for notification of status changes.';
  $webhook->unserializer = 'urlencoded';
  $webhook->processor = 'cyclone_processor_status';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['cyclone_notification_status'] = $webhook;

  return $export;
}
