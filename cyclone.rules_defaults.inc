<?php
/**
 * @file
 * cyclone.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function cyclone_default_rules_configuration() {
  $items = array();
  $items['rules_cyclone_create_site_once_created'] = entity_import('rules_config', '{ "rules_cyclone_create_site_once_created" : {
      "LABEL" : "Cyclone create site once created",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "cyclone" ],
      "ON" : { "node_insert--cyc_site" : { "bundle" : "cyc_site" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:field-cyc-status" ], "value" : "initialized" } },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_cyc_source" } },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_cyc_builder" } },
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_cyc_provider" } }
      ],
      "DO" : [
        { "cyclone_actions_create_site" : { "cyc-site" : [ "node" ] } },
        { "drupal_message" : { "message" : "The site creation process has now begun. It usually takes a few minutes to complete. Please check your email for further instructions." } }
      ]
    }
  }');
  $items['rules_cyclone_site_create_failed_email'] = entity_import('rules_config', '{ "rules_cyclone_site_create_failed_email" : {
      "LABEL" : "Cyclone site create failed email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "cyclone" ],
      "REQUIRES" : [ "rules", "cyclone" ],
      "ON" : { "cyclone_site_notification" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "cyc-site" ],
            "type" : { "value" : { "cyc_site" : "cyc_site" } }
          }
        },
        { "entity_is_of_type" : { "entity" : [ "cyc-notification" ], "type" : "field_collection_item" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "cyc-notification" ],
            "type" : "field_collection_item",
            "bundle" : { "value" : { "field_cyc_notifications" : "field_cyc_notifications" } }
          }
        },
        { "data_is" : {
            "data" : [ "cyc-notification:field-cyc-notification-type" ],
            "value" : "create-failed"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[cyc-site:author:mail]",
            "subject" : "[cyc-site:title] create failure",
            "message" : "Dear [cyc-site:author],\\r\\n\\r\\nThere has been a problem cloning your site. An administrator will address this issue shortly.\\r\\n[cyc-site:url]\\r\\n\\r\\nThanks for you patience whilst we spin up a site for you.\\r\\n\\r\\n[site:name]\\r\\n[site:url]",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_cyclone_site_created_email'] = entity_import('rules_config', '{ "rules_cyclone_site_created_email" : {
      "LABEL" : "Cyclone site created email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "cyclone" ],
      "REQUIRES" : [ "rules", "cyclone" ],
      "ON" : { "cyclone_site_notification" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "cyc-site" ],
            "type" : { "value" : { "cyc_site" : "cyc_site" } }
          }
        },
        { "entity_is_of_type" : { "entity" : [ "cyc-notification" ], "type" : "field_collection_item" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "cyc-notification" ],
            "type" : "field_collection_item",
            "bundle" : { "value" : { "field_cyc_notifications" : "field_cyc_notifications" } }
          }
        },
        { "data_is" : {
            "data" : [ "cyc-notification:field-cyc-notification-type" ],
            "value" : "created"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[cyc-site:author:mail]",
            "subject" : "[cyc-site:title] [cyc-notification:field-cyc-notification-title]",
            "message" : "Dear [cyc-site:author],\\r\\n\\r\\nCongratulations! Your new site has been successfully created and is ready for use. Your website is available at the following URL.\\r\\n[cyc-site:field-cyc-website:url]\\r\\n\\r\\nIn order to log into the site, please use the following one time login link. You will then be asked to enter a password and confirmation for future use of the site.\\r\\n[login-url:value]\\r\\n\\r\\nYou can review the status of your site at any time here:\\r\\n[cyc-site:url]\\r\\n\\r\\n\\r\\nThanks for spinning up a new site with us.\\r\\n\\r\\n[site:name]\\r\\n[site:url]",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_cyclone_site_destroy_failed_email'] = entity_import('rules_config', '{ "rules_cyclone_site_destroy_failed_email" : {
      "LABEL" : "Cyclone site destroy failed email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "cyclone" ],
      "REQUIRES" : [ "rules", "cyclone" ],
      "ON" : { "cyclone_site_notification" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "cyc-site" ],
            "type" : { "value" : { "cyc_site" : "cyc_site" } }
          }
        },
        { "entity_is_of_type" : { "entity" : [ "cyc-notification" ], "type" : "field_collection_item" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "cyc-notification" ],
            "type" : "field_collection_item",
            "bundle" : { "value" : { "field_cyc_notifications" : "field_cyc_notifications" } }
          }
        },
        { "data_is" : {
            "data" : [ "cyc-notification:field-cyc-notification-type" ],
            "value" : "destroy-failed"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[cyc-site:author:mail]",
            "subject" : "[cyc-site:title] destroy failed",
            "message" : "Dear [cyc-site:author],\\r\\n\\r\\nYour website was scheduled to be destroyed, however, there has been a problem removing it from the system. An administrator will address this issue shortly.\\r\\n[cyc-site:url]\\r\\n\\r\\nThanks for spinning up a site with us.\\r\\n\\r\\n[site:name]\\r\\n[site:url]\\r\\n",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_cyclone_site_destroyed_email'] = entity_import('rules_config', '{ "rules_cyclone_site_destroyed_email" : {
      "LABEL" : "Cyclone site destroyed email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "cyclone" ],
      "REQUIRES" : [ "rules", "cyclone" ],
      "ON" : { "cyclone_site_notification" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "cyc-site" ],
            "type" : { "value" : { "cyc_site" : "cyc_site" } }
          }
        },
        { "entity_is_of_type" : { "entity" : [ "cyc-notification" ], "type" : "field_collection_item" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "cyc-notification" ],
            "type" : "field_collection_item",
            "bundle" : { "value" : { "field_cyc_notifications" : "field_cyc_notifications" } }
          }
        },
        { "data_is" : {
            "data" : [ "cyc-notification:field-cyc-notification-type" ],
            "value" : "destroyed"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[cyc-site:author:mail]",
            "subject" : "[cyc-site:title] has been destroyed",
            "message" : "Dear [cyc-site:author],\\r\\n\\r\\nYour website has been destroyed. You will not be able to use it any longer.\\r\\n[cyc-site:url]\\r\\n\\r\\nThanks for spinning up a site with us.\\r\\n\\r\\n[site:name]\\r\\n[site:url]\\r\\n",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_cyclone_site_notification_email'] = entity_import('rules_config', '{ "rules_cyclone_site_notification_email" : {
      "LABEL" : "Cyclone site notification email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "cyclone" ],
      "REQUIRES" : [ "rules", "cyclone" ],
      "ON" : { "cyclone_site_notification" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "cyc-site" ],
            "type" : { "value" : { "cyc_site" : "cyc_site" } }
          }
        },
        { "entity_is_of_type" : { "entity" : [ "cyc-notification" ], "type" : "field_collection_item" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "cyc-notification" ],
            "type" : "field_collection_item",
            "bundle" : { "value" : { "field_cyc_notifications" : "field_cyc_notifications" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[site:mail]",
            "subject" : "Cyclone: [cyc-site:title] [cyc-notification:field-cyc-notification-title]",
            "message" : "A notification has occurred for [cyc-site:title].\\r\\n\\r\\nTitle: [cyc-notification:field-cyc-notification-title]\\r\\nType: [cyc-notification:field-cyc-notification-type]\\r\\nCreated: [cyc-notification:field-cyc-notification-created]\\r\\nBuild URL: [cyc-notification:field-cyc-notification-build:url]\\r\\nNotifier: [cyc-notification:field-cyc-notification-ip]\\r\\nLogin URL: [login-url:value]\\t\\r\\n\\r\\nSee full site details at:\\r\\n[cyc-site:url]\\r\\n\\r\\nThe website is at:\\r\\n[cyc-site:field-cyc-website:url]\\r\\n\\r\\n\\r\\nThe Cyclone Notifier\\r\\n\\r\\n[site:name]\\r\\n[site:url]",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
