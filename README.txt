Cyclone Drupal Module
=====================

Cyclone is a module which allows users to create sites on remote providers.

At its core it provides a cyc_site content type which stores all the information
required to kick off jobs to build sites on remote servers.

Sites require a few basic fields to be properly configured before they will be
build properly. These are the source, provider and builder. Data for these
values are stored in the cyclone_template (field collection) and are copied
across to the cyc_site (node) when an individual site instance is created.

### Source

There are many ways to build a site depending on the provider. Sites can be
cloned, built from a make file, imported from an archive, installed from a
platform, etc. These "actions" go into building up a source, which is a
crucial part of the configuration for a cyc_site. Each provider is able to
validate a source entered in forms to ensure that it has been implemented.

### Provider

A provider represents a hosting provider, an ISP with Drupal platform. Providers
each have their own configuration mainly to determine server hostname and
authentication details. Cyclone currently supports Aegir and Pantheon providers
with an eye to including Aberdeen and Acquia as their APIs improve.

### Builder

A builder is responsible for actually building the remote sites. Our standard
builder, cyclone_jenkins_builder, targets a remote Jenkins server to do the
build. We also ship am Example builder for debugging.

Each builder should attempt to implement the three standard methods for each of
the providers:
- site_create
- site_destroy


Site customisation
------------------

The typical way for a cyc_site (node) to be created is by a user filling in a
form (available as a block) associated with the cyclone_template_display. The
user enters:
- site title
- site sub domain
- site aliases

These are then combined with values from the cyclone_template (field collection)
to build the site.

The cyclone_template module provides this functionality. Developers are free to
build their own workflows if they desire.


Other concepts
--------------

### Status

Each site has a status depending on the methods which have been run and the
resulting outcomes.
- initialized
- create-started
- create-failed
- created
- destroy-started
- destroy-failed
- destroyed

Sites are moved from status to status through a notifications system. The
cyclone module ships with a "cyclone_notification_status" webhook which listens
out for notifications from builders. Please see the webhook documentation for
more details on how webhooks are implemented.

### Extensions

Payloads delivered to Builders follow a strict structure with various key bits
of config. (Use the cyclone_jenkins_builder module to se this.) We have added the
concept of builder extensions. An extension is an arbitrary data structure keyed
by a class name for the extension handler.

To see this in action have a look at cyclone.extensions.Variables extension which
is populated by the cyclone_preserve module. It injects data into the
field_cyc_extensions field. When the builder processes this param it then passes
teh data off to the cyclone.extensions.Variables python class. This class then
does a series of "drush @target vset" commands on the newly created site.

You should see that this is a powerful way to alter the state of the newly
created site. We have set variables but there are a range of other things you
may wish to do with drush. eg. enabling modules, running arbitrary commands, etc.


Installation
------------

Cyclone can be installed like any other Drupal module -- place it in the
modules directory for your site and enable it on the `admin/build/modules` page.

In order to get the full benefit of Cyclone, you should at least install some
other modules:
- cyclone_template: Provides a site templating mechanism which allows for
multiple sites to be created from one template
- A "builder", such as cyclone_jenkins, which targets a Jenkins server to build
the jobs.
- A "provider", such as cyclone_aegir or cyclone_pantheon which target the
hosting provider you wish to create sites on.

Required modules:


Basic usage
-----------

- Enable Cyclone.
- Enable and configure at least one builder.
- Enable and configure at least one provider.
- Enable the cyclone_template module.
- Create a Template Display, with embedded Template
- View the new template and fill in the title and path info to create a new site.

We recommend trying the cyclone_example_builder (with devel) in the early stages
to see how it is working.

If you use cyclone_jenkins_builder you will need a Jenkins server with
appropriate scripts.


Security Concerns
-----------------

Each provider includes server and authentication information for accessing the
hosting platform. This is obviously very sensitive information as it can be
used to create, destroy and perform other scary actions. You must therefore keep
your originating server secure and be careful with assignment of permissions.

One way to mitigate this risk is too store the credentials of a "secondary"
account used for this purpose. If a user wants to keep a site, the site can
be moved to another account (with different authentication information).

Builders such as cyclone_jenkins_builder will make POSTs to the remote Jenkins
server. This MUST by via SSL as the credentials will be sent with the request.
Further, an access token, which operates as a shared secret, is also sent this
way. The use of SSL is therefore crucial.

Information is also posted back to the originating server to the
cyclone_notification_status webhook. The POST is protected by a "signature" in
the payload. It is OK to call this URL with HTTP as no raw tokens or
authorisation params are passed in plain text. The payloads are generally not
that sensitive. The signature is based on a token which is site specific. This
leaves the door open for replay attacks but this is not considered a big
problem.


Pluggable architecture
----------------------

Cyclone has been built to be extensible. We user three types of plugins to do
this.

### cyclone_provider

Represents a hosting provider. Extending CycloneProvider will make things easier
for you when it comes time to build your own.

If you represent a hosting provider and would like to see Cyclone integrated
with your platform, please contact Morpht who will be able to help you.

### cyclone_builder

Represents a builder which is responsible for implementing the main create and
destroy methods on the various providers. This is a large task which
requires knowhow for each of the providers.

The cyclone_example_builder is helpful for debugging. It doesn't actually do
anything useful apart from minimally implement the methods and report the payload.
Turn on devel module so that you are able to view the payload being sent off.

The cyclone_jenkins_builder is the reference implementation for cyclone. It works
by building a request and sending a JSON payload across to a job on a Jenkins
server. The job passes off the request to Fabric and python classes to handle
the interactions with the remote hosting platforms. Once a job is complete the
cyclone module is notified of the outcome. The site status will then be altered.

### processor

We have used the webhook module to handle notifications back from Jenkins
(cyclone_jenkins_builder) and the newly created site (cyclone_preserve_client).
Webhook enables us to implement different payload processors to handle these
use cases.

Generally a processor for cyclone will, create a base notification for the site
and then perform special actions based on the contents of the data param. It
is also good practice to trigger a rule as well.

Support
-------

This project has been developed by Morpht. If you would like to engage us to help
you implement or extend cyclone, please get in touch with us.

contact@morpht.com


Maintainers
-----------

- Murray Woodman
- Marji Cermak
- Ivan Zugec
